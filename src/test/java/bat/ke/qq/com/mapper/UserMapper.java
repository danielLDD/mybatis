package bat.ke.qq.com.mapper;/*
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　┻　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　永无BUG 　┣┓
 * 　　　　┃　　如来保佑　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┗┻┛　┗┻┛
 * 源码学院-只为培养BAT程序员而生
 * bat.ke.qq.com
 * Monkey老师
 */


import bat.ke.qq.com.pojo.User;

public interface UserMapper {
  public User getUser(Integer id);

}
