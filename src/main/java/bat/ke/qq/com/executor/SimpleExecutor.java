package bat.ke.qq.com.executor;


import bat.ke.qq.com.binding.MapperMethod;
import bat.ke.qq.com.session.Configuration;
import bat.ke.qq.com.statement.StatementHandler;

public class SimpleExecutor implements Executor {
  private Configuration configuration;

  public SimpleExecutor(Configuration configuration) {
    this.configuration = configuration;
  }

  @Override
  public <T> T query(MapperMethod method, Object parameter) throws Exception
       {
    StatementHandler statementHandler = new StatementHandler(configuration);
    return statementHandler.query(method,parameter);
  }
}
