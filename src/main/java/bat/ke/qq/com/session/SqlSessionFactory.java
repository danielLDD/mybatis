package bat.ke.qq.com.session;

import bat.ke.qq.com.executor.SimpleExecutor;

public class SqlSessionFactory {



  public SqlSession openSession(Configuration configuration) {
    return  new DefaultSqlSession(configuration,new SimpleExecutor(configuration));
  }

}
